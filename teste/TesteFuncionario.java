package teste;
import Modelo.FuncionarioGoverno;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TesteFuncionario {
	FuncionarioGoverno umFuncionario = new FuncionarioGoverno();
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testNome() {
		umFuncionario.setNome("Horacio Cruz");
		assertEquals("Horacio Cruz", umFuncionario.getNome());
	}

	@Test
	public void testCodigo() {
		umFuncionario.setCodigo("00.0000");
		assertEquals("00.0000", umFuncionario.getCodigo());
	}



}
