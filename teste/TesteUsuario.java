package teste;
import junit.framework.Assert;
import Modelo.Usuario;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TesteUsuario {
	Usuario umUsuario = new Usuario();
	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testUsuario() {
		umUsuario.setNome("Lucas Vilela");
		assertEquals("Lucas Vilela", umUsuario.getNome());
	}
	@Test
	public void testCidade() {
		umUsuario.setCidade("Brasilia");
		assertEquals("Brasilia", umUsuario.getCidade());
	}
	@Test
	public void testEstado() {
		umUsuario.setEstado("DF");
		assertEquals("DF", umUsuario.getEstado());
	}
}
