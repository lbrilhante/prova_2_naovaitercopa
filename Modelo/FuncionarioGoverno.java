package Modelo;

public class FuncionarioGoverno {
	private String Nome;
	private String Codigo;
	public FuncionarioGoverno(){
		
	}
	
	public FuncionarioGoverno(String nome, String codigo) {
		Nome = nome;
		Codigo = codigo;
	}
	
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	
	public String getCodigo() {
		return Codigo;
	}
	public void setCodigo(String codigo) {
		Codigo = codigo;
	}
	
}
