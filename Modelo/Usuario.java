/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author alunos
 */
public class Usuario {
    private String Nome;
    private String Cidade;
    private String Estado;
    private ArrayList<String> Coments;
    //coments será retirado da string coment classe comentario que for feito
    public Usuario(){
    	
    }
    public Usuario(String Nome, String Cidade, String Estado) {
        this.Nome = Nome;
        this.Cidade = Cidade;
        this.Estado = Estado;
    }

    public String getCidade() {
        return Cidade;
    }

    public void setCidade(String Cidade) {
        this.Cidade = Cidade;
    }

    public ArrayList<String> getComents() {
        return Coments;
    }

    public void setComents(ArrayList<String> Coments) {
        this.Coments = Coments;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }
    
}
