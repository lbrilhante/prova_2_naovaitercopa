package Modelo;
import java.util.ArrayList;
public class FuncionarioAvaliador extends FuncionarioGoverno{

	private ArrayList<TopicosDeProtesto> TopicosParaAvaliar;

	public FuncionarioAvaliador(String nome, String codigo,
			ArrayList<TopicosDeProtesto> topicosParaAvaliar) {
		super(nome, codigo);
		this.TopicosParaAvaliar = topicosParaAvaliar;
	}

	public ArrayList<TopicosDeProtesto> getTopicosParaAvaliar() {
		return TopicosParaAvaliar;
	}

	public void setTopicosParaAvaliar(
			ArrayList<TopicosDeProtesto> topicosParaAvaliar) {
		TopicosParaAvaliar = topicosParaAvaliar;
	}
	
}
