package Modelo;
import java.util.ArrayList;
public class TopicosDeProtesto {
	private ArrayList<Comentario> Protestos;
	private String Conteudo;
	
	
	public TopicosDeProtesto(ArrayList<Comentario> protestos, String conteudo) {
		Protestos = protestos;
		Conteudo = conteudo;
	}
	public ArrayList<Comentario> getProtestos() {
		return Protestos;
	}
	public void setProtestos(ArrayList<Comentario> protestos) {
		Protestos = protestos;
	}
	public String getConteudo() {
		return Conteudo;
	}
	public void setConteudo(String conteudo) {
		Conteudo = conteudo;
	}
	
}
