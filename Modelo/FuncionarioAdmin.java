package Modelo;
import java.util.ArrayList;
public class FuncionarioAdmin extends FuncionarioGoverno{
	
	private ArrayList<TopicosDeProtesto> TopicosParaResponder;

	public FuncionarioAdmin(String nome, String codigo,ArrayList<TopicosDeProtesto> topicosParaResponder) {
		super(nome, codigo);
		this.TopicosParaResponder = topicosParaResponder;
	}

	public ArrayList<TopicosDeProtesto> getTopicosParaResponder() {
		return TopicosParaResponder;
	}

	public void setTopicosParaResponder(
			ArrayList<TopicosDeProtesto> topicosParaResponder) {
		TopicosParaResponder = topicosParaResponder;
	}
	
	
}
