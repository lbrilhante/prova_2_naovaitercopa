/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author alunos
 */
public class Comentario {
    private String coment;
    private Usuario Usuario;
    public Comentario(){
    }
    public Comentario(String coment, Modelo.Usuario Usuario) {
        this.coment = coment;
        this.Usuario = Usuario;
    }

    public Modelo.Usuario getUsuario() {
        return Usuario;
    }

    public void setUsuario(Modelo.Usuario Usuario) {
        this.Usuario = Usuario;
    }

    public String getComent() {
        return coment;
    }

    public void setComent(String coment) {
        this.coment = coment;
    }
    
}
