/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author alunos
 */
public class ForumDeDiscursao {
    private ArrayList<Comentario> Posts;
    //Lista de comentarios do forum de discursão
    private String Assunto;
    
    public ForumDeDiscursao(ArrayList<Comentario> Posts, String Assunto) {
        this.Posts = Posts;
        this.Assunto = Assunto;
    }

    public String getAssunto() {
        return Assunto;
    }

    public void setAssunto(String Assunto) {
        this.Assunto = Assunto;
    }

    public ArrayList<Comentario> getPosts() {
        return Posts;
    }

    public void setPosts(ArrayList<Comentario> Posts) {
        this.Posts = Posts;
    }
    
}
