/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author alunos
 */
public class UsuarioModerador extends Usuario{
private ArrayList<ForumDeDiscursao> Foruns;

    public UsuarioModerador(ArrayList<ForumDeDiscursao> Foruns, String Nome, String Cidade, String Estado) {
        super(Nome, Cidade, Estado);
        this.Foruns = Foruns;
    }

    public ArrayList<ForumDeDiscursao> getForuns() {
        return Foruns;
    }

    public void setForuns(ArrayList<ForumDeDiscursao> Foruns) {
        this.Foruns = Foruns;
    }
    
    
}
